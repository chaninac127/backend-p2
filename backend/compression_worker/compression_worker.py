#!/usr/bin/env python3

import sys
import os
import logging
import json
import uuid
from os import listdir
from os.path import isfile,join
import tarfile
from pymongo import MongoClient
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyExists, BucketAlreadyOwnedByYou)
import redis
import requests

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:compress'

INSTANCE_NAME = uuid.uuid4().hex

MONGO = os.getenv('MONGO','localhost')
myclient = MongoClient(f'mongodb://{MONGO}',27017)
buckets = myclient["buckets_db"]

MINIO_URL = os.getenv('MINIO_URL','localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

minioClient = Minio(f'{MINIO_URL}:9000',
                    access_key=MINIO_ACCESS_KEY,
                    secret_key=MINIO_SECRET_KEY,
                    secure=False)

WEB_CONTROLLER = os.getenv("WEB_CONTROLLER","localhost")

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    STATUS_QUEUE = 'queue:status'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf-8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def execute_task(log, task):
    sid = task.get("sid")
    originalname = task.get("originalname")
    bucketname = task.get("bucketname")
    LOG.info("-------------------------------------------------------")
    LOG.info("sid : %s", sid)
    LOG.info("originalname : %s", originalname)
    LOG.info("bucketname : %s", bucketname)
    LOG.info("-------------------------------------------------------")
    LOG.info("===== WORKING ON %s (%s) ======",bucketname,originalname)
    bucket = buckets[bucketname]
    info_entry = bucket.find_one({},{'_id':0})

    to_status_queue = json.dumps({
        'originalname' : originalname,
        'bucketname' : bucketname,
        'sid' : sid,
        'status' : "recompressing"
    })
    RedisResource.conn.rpush(
        RedisResource.STATUS_QUEUE,
        to_status_queue
    )

    update = ({"$set": {"status": "recompressing"}})
    bucket.update_one(info_entry,update)
    LOG.info("============= mongo: status of bucket %s (%s) changed to recompressing =============",bucketname,originalname)
    
    #get the txts from minio using prefix txt_ which are add to txt files during translating process and get the originalname using mongo
    txtsInBucket = minioClient.list_objects(bucketname,prefix="txt_",recursive=False)
    for txt in txtsInBucket:
        txt_name = txt.object_name
        LOG.info("================== FILE NAME : %s ====================",txt_name)
        try:
            item_entry = bucket.find_one({"filename": txt_name},{"_id":0})
            original_txt_name = item_entry['originalname']
            minioClient.fget_object(bucketname,txt_name,f"./{bucketname}/texts/{original_txt_name}")
        except ResponseError as err:
            LOG.info("Error : %s",err)
            print(err)

    LOG.info("original name : %s",originalname)

    compressed_dest = f"./{bucketname}/compressed/"
    if not os.path.exists(compressed_dest):
        os.makedirs(compressed_dest)

    # the compressing part
    with tarfile.open(f"./{bucketname}/compressed/{originalname}","w:gz") as tar:
        tar.add(f"./{bucketname}/texts/",arcname=os.path.basename(f"./{bucket}/texts/"))
    try:
        minioClient.fput_object(bucketname,f"repacked_{originalname}",f"./{bucketname}/compressed/{originalname}")
    except ResponseError as err:
        LOG.info("Error : %s",err)
        print(err)

    DOWNLOAD_URL = f"/main/download/{bucketname}"

    to_status_queue = json.dumps({
        'originalname' : originalname,
        'bucketname' : bucketname,
        'sid' : sid,
        'download_url' : DOWNLOAD_URL,
        'status' : "completed"
    })
    RedisResource.conn.rpush(
        RedisResource.STATUS_QUEUE,
        to_status_queue
    )
    
    info_entry = bucket.find_one({},{'_id':0})
    update = ({"$set": {"status": "recompressed"}})
    bucket.update_one(info_entry,update)
    LOG.info("============= mongo: status of bucket %s (%s) changed to recompressed =============",bucketname,originalname)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        QUEUE_NAME, 
        lambda task_descr: execute_task(named_logging, task_descr))

if __name__ == '__main__':
    # LOG.info("WORKER WOKRING!!!!!!")
    main()


    # docker-compose exec -it <name> ~/bin/bash