#!/usr/bin/env python3

import sys
import os
import logging
import json
import uuid
from os import listdir
from os.path import isfile,join
import tarfile
import redis
from pymongo import MongoClient
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyExists, BucketAlreadyOwnedByYou)
import requests

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
LOG.info("REDIS_QUEUE_LOCATION : ",REDIS_QUEUE_LOCATION)
QUEUE_NAME = 'queue:extract'

MONGO = os.getenv('MONGO','localhost')
myclient = MongoClient(f'mongodb://{MONGO}',27017)
buckets = myclient["buckets_db"]

MINIO_URL = os.getenv('MINIO_URL','localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

minioClient = Minio(f'{MINIO_URL}:9000',
                    access_key=MINIO_ACCESS_KEY,
                    secret_key=MINIO_SECRET_KEY,
                    secure=False)

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    TRANSLATE_QUEUE = 'queue:translate'
    STATUS_QUEUE = 'queue:status'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf-8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def execute_task(log, task):
    sid = task.get("sid")
    originalname = task.get("originalname")
    bucketname = task.get("bucketname")
    LOG.info("===== WORKING ON %s (%s) ======",bucketname,originalname)
    bucket = buckets[bucketname]
    info_entry = bucket.find_one({},{'_id':0})
    update = ({"$set": {"status": "extracting"}})
    bucket.update_one(info_entry,update)
    LOG.info("============= mongo: status of bucket %s (%s) changed to extracting =============",bucketname,originalname)

    #make a folder that store the original tar from minio first
    if not os.path.exists("/original/{}".format(bucketname)):
        LOG.info("make a new directory")
        os.makedirs(f"./original/{bucketname}")
    
    #get the original tar.gz from minio
    try:
        minioClient.fget_object(bucketname,f'original_{originalname}',f"./original/{bucketname}/original_{originalname}")
    except ResponseError as err:
        LOG.info("Error : %s",err)
        print(err)

    LOG.info("+++++++++++++++++++++++++++++ FILE IN TOLOG ++++++++++++++++++++++++++++++++++")
    toLog = [f for f in listdir("./original/{}/".format(bucketname)) if isfile(join("./original/{}/".format(bucketname),f))]
    for file in toLog:
        print("FILE IN DIRECTORY : ",file)
        LOG.info("FILE IN DIRECTORY : %s",file)
    LOG.info("+++++++++++++++++++++++++++++ END FILE IN TOLOG ++++++++++++++++++++++++++++++++++")


    # extract the file
    filename = f"./original/{bucketname}/original_{originalname}"
    tar = tarfile.open(filename)
    if not os.path.exists(f'./{bucketname}/extracted'):
        os.makedirs(f'./{bucketname}/extracted')
    tar.extractall(path="./{}/extracted/".format(bucketname))
    tar.close()
    
    #get the total file count
    status_entry = bucket.find_one({},{'_id':0})
    extracted = [f for f in listdir("./{}/extracted/".format(bucketname)) if isfile(join("./{}/extracted/".format(bucketname),f)) and not f.startswith(".")]
    totalfile = len(extracted)
    count = 0

    # enqueue to update status
    status_json = json.dumps({
        'sid' : sid,
        'bucketname' : bucketname,
        'originalname' : originalname,
        'status' : 'extracted'
    })

    RedisResource.conn.rpush(
        RedisResource.STATUS_QUEUE,
        status_json
    )

    #upload the pdfs back into minio
    for files in extracted:
        try:
            name, ext = os.path.splitext(files)
            minioClient.fput_object(bucketname,f"pdf_{files}",f"./{bucketname}/extracted/{files}")
            bucket.insert_one(({"filename": f"pdf_{files}","originalname":files,"name": name,"extension":ext}))
            count+=1
            if (count == totalfile):
                to_translate_packed = json.dumps({
                    'sid' : sid,
                    'originalname' : originalname,
                    'bucketname' : bucketname,
                    'extractedcount' : totalfile
                })
                RedisResource.conn.rpush( #put a status update into status queue
                    RedisResource.TRANSLATE_QUEUE,
                    to_translate_packed
                )
        except ResponseError as err:
            print(err)
            LOG.info("Error : %s",err)
    new_entry = ({"$set": {"totalfile":totalfile,"extracted": extracted, "status" : "extracted"}})
    bucket.update_one(status_entry,new_entry)

    LOG.info("============= mongo: status of bucket %s (%s) changed to extracted =============",bucketname,originalname)
    #send the bucketname to translate queue
    # requests.post("http://localhost:5001/translate/{}".format(bucketname))

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        QUEUE_NAME, 
        lambda task_descr: execute_task(named_logging, task_descr))

if __name__ == '__main__':
    LOG.info("WORKER WOKRING!!!!!!")
    main()



