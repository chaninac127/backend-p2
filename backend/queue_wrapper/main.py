#!/usr/bin/env python3

#go read l3-work-queue/simple-redisq-v2
#might need multiple?

from random import choice
from string import ascii_lowercase
import os
import json
import redis
import requests
import logging
from flask_cors import CORS
from flask_socketio import SocketIO,send,join_room,leave_room
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyExists, BucketAlreadyOwnedByYou)
from pymongo import MongoClient
from flask import Flask, jsonify, request, abort, render_template, send_from_directory, Response
from werkzeug.utils import secure_filename

LOG = logging.getLogger(name='main-srv')
LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

# taskdict = {}
sid_list = []

EXPOSE_PORT = int(os.getenv("PORT", 5001))


app = Flask(__name__)
CORS(app)
socketio = SocketIO(app)
# app.config['MONGO_URI'] = os.getenv("MONGO_URI","mongodb://localhost:27017/buckets_db")

MONGO = os.getenv('MONGO','localhost')
myclient = MongoClient(f'mongodb://{MONGO}',27017)
buckets = myclient["buckets_db"]

MINIO_URL = os.getenv('MINIO_URL','localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

minioClient = Minio(f'{MINIO_URL}:9000',
                    access_key=MINIO_ACCESS_KEY,
                    secret_key=MINIO_SECRET_KEY,
                    secure=False)
# mongo = PyMongo(app)
# db = client['buckets_db']

#redis resource with queue name for the one needed
class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')

    EXTRACT_QUEUE = 'queue:extract'
    # COMPRESS_QUEUE = 'queue:compress'
    # TRANSLATE_QUEUE = 'queue:translate'
    STATUS_QUEUE = 'queue:status'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_= port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

#extract queue
# @app.route("/extract/<bucketname>")
# def extract(bucketname):

#     #upload tar and extract
#     extractjob = {'bucketname':bucketname}
#     json_packed = json.dumps(extractjob)
#     RedisResource.conn.rpush(
#         RedisResource.EXTRACT_QUEUE,
#         json_packed
#     )
#     return jsonify({'status':'OK','queue':'extract'})


#translate queue
# @app.route("/translate/<bucketname>")
# def translate(bucketname):

#     #translate all pdf to txt
#     translatejob = {'bucketname':bucketname}
#     json_packed = json.dumps(translatejob)
#     RedisResource.conn.rpush(
#         RedisResource.TRANSLATE_QUEUE,
#         json_packed
#     )
#     return jsonify({'status':'OK','queue':'translate'})

#compress queue
# @app.route("/compress/<bucketname>")
# def compress(bucketname):

#     #compress the txt files into tar.gz
#     compressjob = {'bucketname':bucketname}
#     json_packed = json.dumps(compressjob)
#     RedisResource.conn.rpush(
#         RedisResource.COMPRESS_QUEUE,
#         json_packed
#     )
#     return jsonify({'status':'OK','queue':'compress'})

# #status queue (might not actually need)
# @app.route("/status/<bucketname>")
# def update_status(bucketname):
#     update_content = {'bucketname':bucketname,'status':status}
#     LOG.info('update_content : %s',update_content)
#     json_packed = json.dumps(update_content)
#     RedisResource.conn.rpush(
#         RedisResource.STATUS_QUEUE,
#         json_packed
#     )
#     return jsonify({'status':'OK','queue':'status'})


#implement vue dropzone? let read the tutorial and stuffs today
# @app.route('/upload')
# def upload_file():
#    return render_template('upload.html')

# @socketIO.on('join')
# def on_join():
#     print("Joined room")
#     socketIO.send()

# @socket_io.on('connect', namespace='/jobs')
# def connect(sid, environ):
#     sid_list.append(sid)
#     LOG.info('connected from %s',sid)
#     LOG.info('sid_list : %s',sid_list)

# @socket_io.on('add job', namespace='/jobs')
# def add_job(sid, data):
#     from datetime import datetime
#     LOG.info("job %r from %s", data, sid)
#     data['timestamp'] = datetime.now().isoformat()
#     socket_io.emit('tojobqueue', data=data, room=None)

@app.route("/main/download/<bucketname>")
def download_finished(bucketname):

    bucket = buckets[bucketname]
    item_entry = bucket.find_one({},{'_id':0})
    if (item_entry['status'] == "recompressed"):
        LOG.info("========================= DOWNLOADING %s ===========================",bucketname)
        originalname = item_entry['original']
        headers = {'Content-Disposition': f'attachment; filename={originalname}'}
        resp = minioClient.get_object(bucketname,f"repacked_{originalname}")
        return Response(response=resp, headers=headers)
    return Response(status=404)

@app.route('/send_status', methods=['POST'])
def send_status():
    socketio.emit('status-update', request.json, room=request.json.get('sid'))
    return Response(status=200)

@app.route('/main/uploader', methods = ['POST'])
def uploader():
    if request.method == 'POST':
        # f = request
        print("===================================")
        print("===================================")
        sid = request.args.get('sid')
        print("sid : ",sid)
        sid_list.append(sid)
        print("sid_lsit : ", sid_list)
        print("request data : ", request)
        print("request method : ", request.method)
        print("reqeust files : ",request.files.getlist('uploader'))
        files = request.files.getlist('uploader')
        for file in files:
            filename = file.filename
            print("+++++++++++++++++++++++++++++++++")
            print(file)
            print(filename)
            print(file.headers)
            # with open("./test/"+file.filename,'wb') as local:
            #     local.write(file.read())
            print("+++++++++++++++++++++++++++++++++")
            if(not os.path.exists("./test")):
                os.mkdir("./test")
            file.save("./test/"+filename)
            bucketname = ''
            nameExist = True
            while nameExist :
                try:
                    bucketname = ''.join(choice(ascii_lowercase) for i in range(12))
                    if(not minioClient.bucket_exists(bucketname)):
                        nameExist = False
                except ResponseError as err:
                    print(err)
                    LOG.info("ERROR : %s",err)
            bucket = buckets[bucketname]
            bucket.insert_one(({'bucketname':bucketname,'status':'in queue','original':filename,'totalfile':0,'count':0,'extracted':[]}))
            first_entry = bucket.find_one({},{'_id':0})
            LOG.info("first_entry : %s",first_entry)
            print("bucketname: ",bucketname)
            try:
                minioClient.make_bucket(bucketname)
            except BucketAlreadyExists as err:
                print(err)
            except ResponseError as err:
                print(err)
            except BucketAlreadyOwnedByYou as err:
                print(err)
            else:
                try:
                    minioClient.fput_object(bucketname,f"original_{filename}","./test/"+filename)
                except ResponseError as err:
                    print(err)

            to_status_queue = json.dumps({
                'originalname' : filename,
                'bucketname' : bucketname,
                'sid' : sid,
                'status' : "waiting for extraction"
            })
            RedisResource.conn.rpush(
                RedisResource.STATUS_QUEUE,
                to_status_queue
            )
            
            to_extract_queue = json.dumps({ 
                'bucketname' : bucketname,
                'originalname' : filename,
                'sid' : sid
            })
            RedisResource.conn.rpush(
                RedisResource.EXTRACT_QUEUE,
                to_extract_queue
            )

            os.remove("./test/"+filename)
        print("===================================")
        print("===================================")
    return Response(status=200)
		
if __name__ == '__main__':
    socketio.run(app,host="0.0.0.0",port=EXPOSE_PORT)
    # app.run(debug = True)

# try:
#     minioClient.make_bucket("testingbucket2")
# except BucketAlreadyExists as err :
#     print(err)
# except ResponseError as err:
#     print(err)
# except BucketAlreadyOwnedByYou as err:
#     print(err)
# else:
#     try:
#         minioClient.fput_object("testingbucket2","testingobject","./Backend/p2/test/")
#     except ResponseError as err:
#         print(err)

# docker run -p 9000:9000 --name minio1 \
#   -e "MINIO_ACCESS_KEY=admin" \
#   -e "MINIO_SECRET_KEY=password" \
#   -v /mnt/data:/data \
#   -v /mnt/config:/root/.minio \
#   minio/minio server /data