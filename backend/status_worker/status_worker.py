#!/usr/bin/env python3

import sys
import os
from os import listdir
from os.path import isfile,join
import logging
import json
import uuid
import subprocess
import redis
import requests
from pymongo import MongoClient
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyExists, BucketAlreadyOwnedByYou)

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:status'

MONGO = os.getenv('MONGO','localhost')
myclient = MongoClient(f'mongodb://{MONGO}',27017)
buckets = myclient["buckets_db"]

MINIO_URL = os.getenv('MINIO_URL','localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

minioClient = Minio(f'{MINIO_URL}:9000',
                    access_key=MINIO_ACCESS_KEY,
                    secret_key=MINIO_SECRET_KEY,
                    secure=False)

WEB_CONTROLLER = os.getenv("WEB_CONTROLLER", "localhost")
SEND_STATUS_URL = f'http://{WEB_CONTROLLER}:5001/send_status'

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf-8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def execute_task(log, task):
    requests.post(SEND_STATUS_URL,json=task)
    # bucketname = task.get("bucketname")
    # LOG.info("===== WORKING ON %s ======",bucketname)
    # status = task.get("status")
    # bucket = buckets[bucketname]
    # info_entry = bucket.find_one({},{'_id': 0})
    # updating_entry = ({"$set":{"status" : status}})
    # bucket.update_one(info_entry,updating_entry)
    # LOG.info("============= status of bucket %s changed to %s =============",bucketname,status)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        QUEUE_NAME, 
        lambda task_descr: execute_task(named_logging, task_descr))

if __name__ == '__main__':
    # LOG.info("WORKER WOKRING!!!!!!")
    main()