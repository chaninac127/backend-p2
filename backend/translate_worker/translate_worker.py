#!/usr/bin/env python3

import sys
import os
from os import listdir
from os.path import isfile,join
import logging
import json
import uuid
import subprocess
import redis
import requests
from pymongo import MongoClient
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyExists, BucketAlreadyOwnedByYou)

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:translate'

MONGO = os.getenv('MONGO','localhost')
myclient = MongoClient(f'mongodb://{MONGO}',27017)
buckets = myclient["buckets_db"]

MINIO_URL = os.getenv('MINIO_URL','localhost')
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')

minioClient = Minio(f'{MINIO_URL}:9000',
                    access_key=MINIO_ACCESS_KEY,
                    secret_key=MINIO_SECRET_KEY,
                    secure=False)

INSTANCE_NAME = uuid.uuid4().hex

class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    COMPRESS_QUEUE = 'queue:compress'
    STATUS_QUEUE = 'queue:status'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf-8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def execute_task(log, task):
    sid = task.get("sid")
    originalname = task.get("originalname")
    totalfile = task.get("extractedcount")
    bucketname = task.get("bucketname")
    LOG.info("---------------------------------------------------")
    LOG.info("sid : %s", sid)
    LOG.info("totalfile : %s", totalfile)
    LOG.info("bucketname : %s",bucketname)
    LOG.info("originalname(bucket) : %s",originalname)
    LOG.info("---------------------------------------------------")
    LOG.info("===== WORKING ON %s (%s) ======",bucketname,originalname)
    bucket = buckets[bucketname]

    to_status_queue = json.dumps({
        'originalname' : originalname,
        'bucketname' : bucketname,
        'sid' : sid,
        'status' : "translating"
    })
    RedisResource.conn.rpush(
        RedisResource.STATUS_QUEUE,
        to_status_queue
    )

    info_entry = bucket.find_one({},{'_id':0})
    update = ({"$set": {"status": "translating"}})
    bucket.update_one(info_entry,update)
    LOG.info("============= status of bucket %s (%s) changed to translating =============",bucketname,originalname)

    #get the pdfs from minio using the prefit pdf_ which is added to each pdf file at the extraction procress
    pdfsInBucket = minioClient.list_objects(bucketname,prefix="pdf_",recursive=False)
    for pdf in pdfsInBucket:
        object_name_from_minio = pdf.object_name
        LOG.info("FILENAME : %s", object_name_from_minio)
        try:
            item_entry = bucket.find_one({"filename": object_name_from_minio},{"_id":0})
            minioClient.fget_object(bucketname,object_name_from_minio,f"./{bucketname}/extracted/{item_entry['originalname']}")
        except ResponseError as err:
            LOG.info("Error : %s",err)
            print(err)

    #convert them through a loop 
    #BIG QUESTION : how to keep track of x out of y status one...
    extracted = [f for f in listdir("./{}/extracted/".format(bucketname)) if isfile(join("./{}/extracted/".format(bucketname),f))]
    info_entry = bucket.find_one({},{'_id':0})
    maxcount = len(extracted)
    count = 0

    #some checking
    if maxcount == totalfile :
        LOG.info("FILE COUNT CORRECT")
    else:
        LOG.info("FILE COUNT WRONG?")


    to_status_queue = json.dumps({
        'originalname' : originalname,
        'bucketname' : bucketname,
        'sid' : sid,
        'status' : f"translated {count} out of {maxcount} file(s)"
    })
    RedisResource.conn.rpush(
        RedisResource.STATUS_QUEUE,
        to_status_queue
    )

    update = ({"$set": {"count":count,"status": f"translating : {count} out of {maxcount} translated"}})
    bucket.update_one(info_entry,update)

    for pdfs in extracted:

        directory_to_keep_txt = f'./{bucketname}/texts'

        if not os.path.exists(directory_to_keep_txt):
            os.makedirs(directory_to_keep_txt)

        name, ext = os.path.splitext(pdfs)
        source = f"./{bucketname}/extracted/{pdfs}"
        dest = f"./{bucketname}/texts/{name}.txt"
        subprocess.call(["pdftotext","-layout", source, dest])
        
        #this update count looks really bad... should be some way better
        count+=1

        to_status_queue = json.dumps({
            'originalname' : originalname,
            'bucketname' : bucketname,
            'sid' : sid,
            'status' : f"translated {count} out of {maxcount} file(s)"
        })
        RedisResource.conn.rpush(
            RedisResource.STATUS_QUEUE,
            to_status_queue
        )
        if count == maxcount:
            to_status_queue = json.dumps({
                'originalname' : originalname,
                'bucketname' : bucketname,
                'sid' : sid,
                'status' : "translated"
            })
            RedisResource.conn.rpush(
                RedisResource.STATUS_QUEUE,
                to_status_queue
            )
        info_entry = bucket.find_one({},{'_id':0})
        update = ({"$set": {"count":count,"status": f"translating : {count} out of {maxcount} translated"}})
        bucket.update_one(info_entry,update)

    #upload the txts back into minio with txt_ prefix
    txts = [t for t in listdir("./{}/texts/".format(bucketname)) if isfile(join("./{}/texts/".format(bucketname),t))]
    for txt in txts:
        try:
            name, ext = os.path.splitext(txt)
            minioClient.fput_object(bucketname,f"txt_{txt}",f"./{bucketname}/texts/{txt}")
            bucket.insert_one(({"filename": f"txt_{txt}","originalname":txt,"extension":ext}))
        except ResponseError as err:
            LOG.info("Error : %s",err)
            print(err)

    info_entry = bucket.find_one({},{'_id':0})
    update = ({"$set": {"status": "translated"}})
    bucket.update_one(info_entry,update)
    LOG.info("============= status of bucket %s (%s) changed to translated =============",bucketname,originalname)

    originalname = task.get('originalname')
    to_compress_queue = json.dumps({
        'originalname' : originalname,
        'bucketname' : bucketname,
        'sid' : sid,
    })
    RedisResource.conn.rpush(
        RedisResource.COMPRESS_QUEUE,
        to_compress_queue
    )
    #send the bucketname to compress queue
    # requests.post("http://localhost:5001/compress/{}".format(bucketname))

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        QUEUE_NAME, 
        lambda task_descr: execute_task(named_logging, task_descr))

if __name__ == '__main__':
    # LOG.info("WORKER WOKRING!!!!!!")
    main()

