#!/bin/bash

docker build -t chanin5781033/nginx ./nginx-proxy

docker build -t chanin5781033/frontend ./frontend/vue_frontend

path_to_make_images='ls -d ./backend/*/'
for path in $path_to_make_images
do
    folder=${path#*/*/}
    folder=${folder%/}

    echo $folder

    docker build -t chanin5781033/$folder $path
done 