#!/bin/bash

kubectl delete deployment translate-worker

kubectl delete deployment status-worker

kubectl delete deployment extract-worker

kubectl delete deployment compression-worker

kubectl delete deployment web-controller

kubectl delete deployment nginx-proxy

kubectl delete deployment frontend

kubectl delete deployment redis

kubectl delete deployment mongo

kubectl delete deployment minio

kubectl delete svc/mongo

kubectl delete svc/minio

kubectl delete svc/frontend

kubectl delete svc/redis

kubectl delete svc/web-controller

kubectl delete svc/nginx-proxy

kubectl delete pvc/mongo

kubectl delete pv/mongo-pv

kubectl delete pv/minio-pv